import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OldProductsComponent } from './old-products.component';

describe('OldProductsComponent', () => {
  let component: OldProductsComponent;
  let fixture: ComponentFixture<OldProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OldProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OldProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
