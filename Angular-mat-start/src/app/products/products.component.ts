import { Component, OnInit, ViewChild } from '@angular/core';
import { ServicesComponent } from '../services/services.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  productLoc: any;

  constructor() { }

  proData :any
  proArr : any[] = []
  ngOnInit() {

    this.getData()

    }

  getData(){
     this.proData = this.sampleProductData();

    console.log(this.proArr)

  }


  sampleProductData(){
    return { "products" : [
      { "Name": "Cheese", "Price" : 2.50, "Location": "Refrigerated foods"},
      { "Name": "Crisps", "Price" : 3, "Location": "the Snack isle"},
      { "Name": "Pizza", "Price" : 4, "Location": "Refrigerated foods"},
      { "Name": "Chocolate", "Price" : 1.50, "Location": "the Snack isle"},
      { "Name": "Self-raising flour", "Price" : 1.50, "Location": "Home baking"},
      { "Name": "Ground almonds", "Price" : 3, "Location": "Home baking"}
    ]}
  }

  myselectedproduct(val: any){
    console.log(val.Location)

    this.productLoc = val.Location
  }
}
