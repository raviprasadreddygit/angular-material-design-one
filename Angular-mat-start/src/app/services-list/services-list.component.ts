import { Component, OnInit, ViewChild  , AfterViewInit} from '@angular/core';
import { ServicesComponent } from '../services/services.component';
import { ProductsComponent } from '../products/products.component';

@Component({
  selector: 'app-services-list',
  templateUrl: './services-list.component.html',
  styleUrls: ['./services-list.component.css']
})
export class ServicesListComponent implements OnInit , AfterViewInit{

  @ViewChild(ServicesComponent) allChildData : ServicesComponent

  constructor() { }

  ngOnInit() {

    console.log(this.allChildData)
  
  }





  ngAfterViewInit(){
  }

}
