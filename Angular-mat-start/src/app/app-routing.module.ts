import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ServicesComponent } from './services/services.component';
import { ProductsComponent } from './products/products.component';
import { AboutComponent } from './about/about.component';

const routes: Routes = [{ path : 'signUp' , component: SignupComponent } ,
                        { path : 'login' , component: LoginComponent },
                        { path : 'home' , component: HomeComponent },
                        { path : 'about' , component: AboutComponent },
                        { path : 'products' , component: ProductsComponent },
                        { path : 'services' , component: ServicesComponent }
                       ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
