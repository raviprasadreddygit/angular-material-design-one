import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent    } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ProductsComponent } from './products/products.component';
import { ServicesComponent } from './services/services.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule } from '@angular/forms';

import {FlexLayoutModule} from '@angular/flex-layout';


import { CommonModule } from '@angular/common';


import { AgmCoreModule } from '@agm/core';


import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';


import  {MyOwnCustomMaterialModule} from './app.material.module';
import { ServicesListComponent } from './services-list/services-list.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { OldProductsComponent } from './old-products/old-products.component';
import { NewProductsComponent } from './new-products/new-products.component'


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ProductsComponent,
    ServicesComponent,
    ServicesListComponent,
    SignupComponent,
    LoginComponent,
    OldProductsComponent,
    NewProductsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
    MyOwnCustomMaterialModule,
    MatGoogleMapsAutocompleteModule,
  
  ],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule { }
