import {MatButtonModule, MatCheckboxModule , MatListModule, MatTabsModule , MatToolbarModule , MatSidenavModule, MatIconModule, MatSelectModule } from '@angular/material';
import { NgModule } from '@angular/core';

//import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';


@NgModule({
  imports: [MatButtonModule, MatCheckboxModule ,MatListModule ,MatTabsModule ,
     MatToolbarModule ,MatSidenavModule ,MatSidenavModule , MatIconModule , MatSelectModule ],

  exports: [MatButtonModule, MatCheckboxModule , MatListModule ,MatTabsModule ,
     MatToolbarModule , MatSidenavModule  , MatSidenavModule , MatIconModule , MatSelectModule],
})
export class MyOwnCustomMaterialModule { }